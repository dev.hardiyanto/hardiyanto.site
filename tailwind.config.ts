const animate = require("tailwindcss-animate");

/** @type {import('tailwindcss').Config} */
module.exports = {
	darkMode: ["class"],
	safelist: ["dark"],
	prefix: "",

	content: [
		"./components/**/*.{js,vue,ts,tsx}",
		"./layouts/**/*.vue",
		"./pages/**/*.vue",
		"./plugins/**/*.{js,ts}",
		"./app.vue",
		"./error.vue",
	],

	theme: {
		container: {
			center: true,
			padding: '2rem',
			screens: {
				'2xl': '1400px'
			}
		},
		extend: {
			screens: {
				xs: '480px'
			},
			content: {
				'year-to-year': '"0000 — 0000"'
			},
			colors: {
				border: 'hsl(var(--border))',
				input: 'hsl(var(--input))',
				ring: 'hsl(var(--ring))',
				background: 'hsl(var(--background))',
				foreground: 'hsl(var(--foreground))',
				primary: {
					'50': '#f5f7fa',
					'100': '#ebeef3',
					'200': '#d1dbe6',
					'300': '#a9bbd0',
					'400': '#7b98b5',
					'500': '#5b7b9c',
					'600': '#415a77',
					'700': '#3a506a',
					'800': '#334559',
					'900': '#2e3b4c',
					DEFAULT: 'hsl(var(--primary))',
					foreground: 'hsl(var(--primary-foreground))'
				},
				tertiary: {
					'50': '#f4f6fb',
					'100': '#e8ecf6',
					'200': '#ccd8eb',
					'300': '#9fb8da',
					'400': '#6b92c5',
					'500': '#4975ae',
					'600': '#375c92',
					'700': '#2d4a77',
					'800': '#294063',
					'900': '#263754',
					DEFAULT: '#1b263b'
				},
				secondary: {
					'50': '#f4f7f9',
					'100': '#ebf1f4',
					'200': '#dae6eb',
					'300': '#c3d5de',
					'400': '#abbfce',
					'500': '#95abbf',
					'600': '#778da9',
					'700': '#6b7e97',
					'800': '#58677b',
					'900': '#4b5664',
					DEFAULT: 'hsl(var(--secondary))',
					foreground: 'hsl(var(--secondary-foreground))'
				},
				destructive: {
					DEFAULT: 'hsl(var(--destructive))',
					foreground: 'hsl(var(--destructive-foreground))'
				},
				muted: {
					DEFAULT: 'hsl(var(--muted))',
					foreground: 'hsl(var(--muted-foreground))'
				},
				accent: {
					DEFAULT: 'hsl(var(--accent))',
					foreground: 'hsl(var(--accent-foreground))'
				},
				popover: {
					DEFAULT: 'hsl(var(--popover))',
					foreground: 'hsl(var(--popover-foreground))'
				},
				card: {
					DEFAULT: 'hsl(var(--card))',
					foreground: 'hsl(var(--card-foreground))'
				},
				chart: {
					'1': 'hsl(var(--chart-1))',
					'2': 'hsl(var(--chart-2))',
					'3': 'hsl(var(--chart-3))',
					'4': 'hsl(var(--chart-4))',
					'5': 'hsl(var(--chart-5))'
				},
				darkest: {
					DEFAULT: 'hsl(var(--darkest))',
					foreground: 'hsl(var(--darkest-foreground))'
				}
			},
			borderRadius: {
				xl: 'calc(var(--radius) + 4px)',
				lg: 'var(--radius)',
				md: 'calc(var(--radius) - 2px)',
				sm: 'calc(var(--radius) - 4px)'
			},
			keyframes: {
				'accordion-down': {
					from: {
						height: '0'
					},
					to: {
						height: 'var(--reka-accordion-content-height)'
					}
				},
				'accordion-up': {
					from: {
						height: 'var(--reka-accordion-content-height)'
					},
					to: {
						height: '0'
					}
				},
				'collapsible-down': {
					from: {
						height: 0
					},
					to: {
						height: 'var(--radix-collapsible-content-height)'
					}
				},
				'collapsible-up': {
					from: {
						height: 'var(--radix-collapsible-content-height)'
					},
					to: {
						height: 0
					}
				}
			},
			animation: {
				'accordion-down': 'accordion-down 0.2s ease-out',
				'accordion-up': 'accordion-up 0.2s ease-out',
				'collapsible-down': 'collapsible-down 0.2s ease-in-out',
				'collapsible-up': 'collapsible-up 0.2s ease-in-out'
			},
			fontFamily: {
				bangers: [
					'Bangers',
					'cursive'
				],
				openSans: [
					'Open Sans',
					'sans-serif'
				],
				roboto: [
					'Roboto',
					'sans-serif'
				],
				redditSans: [
					'Reddit Sans',
					'sans-serif'
				]
			}
		}
	},
	plugins: [animate],
};

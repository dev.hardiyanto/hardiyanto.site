// import { format } from "date-fns";
import type { $Img, ImageModifiers, ImageOptions } from "@nuxt/image";
import type { ActiveHeadEntry, UseSeoMetaInput } from "@unhead/vue";

export default function () {
  return {
    useImg,
    useImgPlaceholder,
    useMeta,
    // useDateFormat,
    // API Used
    useArticles,
    useApi,
  };
}

function useMeta(meta: UseSeoMetaInput): void | ActiveHeadEntry<any> {
  return useSeoMeta({ ...meta });
}

function useImg(
  source: string,
  modifiers?: Partial<ImageModifiers> | undefined,
  options?: ImageOptions | undefined
): string {
  const img = useImage();
  return img(source, modifiers, options);
}

function useImgPlaceholder(): number[] {
  return [50, 25, 75, 5];
}

// function useDateFormat(the_date: string): string {
//   return format(the_date, "MMM dd, yyyy");
// }

async function useArticles(params?: any) {
  return await useApi("article", "articles", "GET", params);
}

async function useApi(
  key: string,
  route: string,
  method:
    | "GET"
    | "HEAD"
    | "PATCH"
    | "POST"
    | "PUT"
    | "DELETE"
    | "CONNECT"
    | "OPTIONS"
    | "TRACE",
  params: any
): Promise<any> {
  return await useLazyAsyncData(key, () =>
    $fetch(`api/${route}`, {
      method: method,
      query: method === "GET" ? params : undefined, // Use query for GET requests
      body: method !== "GET" ? params : undefined, // Use body for other methods
    })
  );
}

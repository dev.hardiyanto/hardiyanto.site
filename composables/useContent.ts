export default function () {
  return {
    about,
    works,
    educations,
    projects,
    side_projects,
  };
}

function about() {
  return "i'm Full-stack web developer who bridges the gap between user-centric design and technical excellence. With a background in PHP, Laravel, and VueJS, I’ve spent 5+ years";
}

function educations() {
  return [
    {
      year_from: '2013',
      year_to: '2017',
      degree: 'Software Engineering',
      school: 'SMKN 1 Cimahi',
      location: 'Cimahi, West Java, Indonesia',
    },
  ]
}

function works() {
  return [
    {
      year_from: '2017',
      year_to: '2018',
      link: 'https://velotek.co.id/',
      position: 'Junior Web Developer',
      company: 'Velocite Technology',
      location: 'Bandung, West Java, Indonesia',
      job_list: [
        "Similar to Torche, responsible for implementing and maintaining government web applications, daily reporting, and feature development.",
        "Perizinan Migas (Report & Maintenance): Continued working on oil and gas licensing systems.",
        "Pencaksilat App: Developed and maintained an app supporting the martial art community.",
      ],
    },
    {
      year_from: '2018',
      year_to: '2019',
      link: 'https://torche.co.id/',
      position: 'Web Developer',
      company: 'Torche Indonesia',
      location: 'Bandung, West Java, Indonesia',
      job_list: [
        "Implemented new web applications for government projects.",
        "Added new features, maintained existing web apps, and provided daily reports.",
        "Caleg2019.id: A platform dedicated to election information and resources.",
        "Perizinan Migas (Migration & Maintenance): Led migration and maintenance efforts for the Ministry of Energy and Mineral Resources' oil and gas licensing platform.",
      ],
    },
    {
      year_from: '2019',
      year_to: '2020',
      position: 'Web Developer',
      company: 'Mirum Agency',
      location: 'South Jakarta (Remote), Jakarta, Indonesia',
      job_list: [
        "Led campaign apps development for major Nestlé products (DANCOW Gum & Fortigro, Milo, Lactogrow).",
        "Implemented SEO strategies to enhance visibility and performance.",
        "Optimized web pages to improve speed, user engagement, and search rankings.",
        "Managed 3rd party integrations for a seamless user experience.",
        "Coordinated the input of articles and event promotions to keep content fresh and up-to-date.",
      ],
    },
    {
      year_from: '2020',
      year_to: '2022',
      position: 'Senior Web Developer',
      company: 'Wundermanthompson Indonesia',
      location: 'South Jakarta (Remote), Jakarta, Indonesia',
      job_list: [
        "Maintained SEO and digital performance for brands like Dancow, Milo, and Lactogrow (Nestlé)",
        "Led several successful campaigns such as 'Dongeng Aku dan Kau,' 'Semangat Siap Sekolah,' and 'Bekal Berenergi.'",
        "Developed and maintained web solutions using DrupalCMS and custom REST API modules.",
        "Collaborated with global brands to deliver impactful digital campaigns that aligned with business goals.",
      ],
    },
    {
      year_from: '2022',
      year_to: 'Now',
      link: 'https://www.tix.id/',
      position: 'Web Developer Associate',
      company: 'TIX ID',
      location: 'South Jakarta (Hybrid), Jakarta, Indonesia',
      job_list: [
        "Managed back-office and event dashboard systems using VueJS and Laravel.",
        "Played a key role in enhancing the front-end of the event ticketing journey.",
        "Spearheaded the revamp of event.tix.id using NuxtJS.",
        "Responsible for maintaining the main platform tix.id built on WordPress.",
        "Mastered end-to-end web architecture from back-end logic to front-end delivery.",
      ],
    },
  ]
}

function side_projects() {
  return [
    // {
    //   year: '2022',
    //   link: 'https://assistant.eferna.com/',
    //   name: 'AI Assistant',
    //   company: 'Eferna',
    // },
  ]
}

function projects() {
  return [
    {
      year: '2022',
      link: 'https://www.naturallyspeakingbyerha.co.id/',
      name: 'Naturally Speaking by Erha',
      company: 'INARY Agency',
    },
    {
      year: '2023',
      link: 'https://www.akudankau.co.id/semangatsiapsekolah',
      name: 'Semangat Siap Sekolah by Dancow (Aku dan Kau)',
      company: 'VML Indonesia',
    },
    {
      year: '2023',
      link: 'https://dermieshelloglow.co.id/',
      name: 'Dermies Hello Glow by Erha',
      company: 'INARY Agency',
    },
    {
      year: '2023',
      link: 'https://dermiescomfortyou.co.id/',
      name: 'Dermies Comfort You by Erha',
      company: 'INARY Agency',
    },
    {
      year: '2023',
      link: 'https://dermiesclearme.co.id/',
      name: 'Dermies Clearme by Erha',
      company: 'INARY Agency',
    },
    {
      year: '2024',
      link: 'https://dermiesmax.co.id/',
      name: 'Dermies Max by Erha',
      company: 'INARY Agency',
    },
  ]
}
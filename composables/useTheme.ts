const useGetTheme = (): string => {
  const storedTheme = localStorage.getItem('hs_theme');
  const prefersDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
  return storedTheme || (prefersDark ? 'dark' : 'light');
};

const useSetTheme = (theme: string) => {
  const html = document.querySelector('html');
  if (html) {
    html.setAttribute('class', theme);
    localStorage.setItem('hs_theme', theme);
  }
};

export default function () {
  let html: HTMLHtmlElement | null;

  onMounted(() => {
    html = document.querySelector('html');
  });

  return {
    useGetTheme,
    useSetTheme,
  }
}

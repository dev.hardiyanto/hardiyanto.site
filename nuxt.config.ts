const PORT = 9001;

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  typescript: {
    typeCheck: true
  },
  css: [
    "~/assets/main.css",
    "~/assets/base/typography.css",
    "~/assets/base/loader.css",
    'remixicon/fonts/remixicon.css',
  ],

  plugins: [
    // "~/plugins/preline.client.ts", "~/plugins/analytics.client.ts"
  ],

  components: [
    {
      path: "~/components",
      pathPrefix: false,
    },
  ],

  runtimeConfig: {
    public: {
      API_BASE_URL: process.env.API_BASE_URL,
      API_TOKEN: process.env.API_TOKEN,
    },
  },

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },

  modules: [
    "@nuxtjs/tailwindcss",
    "@nuxtjs/color-mode",
    "@nuxtjs/i18n",
    "@nuxt/image",
    "@pinia/nuxt",
    // "nuxt-delay-hydration",
    "shadcn-nuxt",
  ],

  shadcn: {
    prefix: '',
    componentDir: './components/ui'
  },

  colorMode: {
    classSuffix: ''
  },

  i18n: {
    vueI18n: "~/lang/i18n.config.ts",
  },

  devServer: {
    port: PORT,
  },

  hooks: {
    "build:done": () => {
      // Buka browser setelah build selesai
      const open = require("open");
      open(`http://localhost:${PORT}`); // Ganti dengan URL yang sesuai jika berbeda
    },
  },

  compatibilityDate: "2024-07-11",
});
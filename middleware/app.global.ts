export default defineNuxtRouteMiddleware((to, from) => {
  const loadingStore = useLoadingStore();

  // Add loader during routing
  loadingStore.startLoading();
  return new Promise<void>((resolve) => {
    setTimeout(() => {
      loadingStore.stopLoading();
      resolve();
    }, 750); // Add a slight delay for smooth transition
  });
})


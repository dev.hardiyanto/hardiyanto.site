export const id = {
  welcome: "Welcome",
  summary: "Saya Irfan Hardiyanto, seorang pengembang web full-stack yang menjembatani kesenjangan antara desain yang berpusat pada pengguna dan keunggulan teknis. Dengan latar belakang di PHP, Laravel, dan VueJS, saya telah menghabiskan lebih dari 5 tahun mengembangkan solusi web inovatif untuk merek-merek seperti Nestlé dan TIXID. Pekerjaan saya adalah menciptakan pengalaman web yang tidak hanya berfungsi dengan sempurna, tetapi juga menangkap esensi dari visi sebuah merek",

  about_title: "Tentang Saya",
  education_title: "Pendidikan",
  work_title: "Pengalaman Kerja",
  project_title: "Proyek",
  side_project_title: "Proyek Sampingan",
}
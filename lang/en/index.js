export const en = {
  welcome: "Welcome",
  summary: "I’m Irfan Hardiyanto, a full-stack web developer who bridges the gap between user-centric design and technical excellence. With a background in PHP, Laravel, and VueJS, I’ve spent 5+ years developing innovative web solutions for brands like Nestlé and TIXID. My work is all about creating web experiences that not only function flawlessly but also capture the essence of a brand’s vision.",

  about_title: "About",
  education_title: "Education",
  work_title: "Work Experience",
  project_title: "Projects",
  side_project_title: "Side Projects",
}
import { en } from './en';
import { id } from './id';

export default defineI18nConfig(() => ({
  legacy: false,
  locale: 'en',
  warnHtmlMessage: false,
  messages: {
    en: { ...en },
    id: { ...id },
  }
}))
